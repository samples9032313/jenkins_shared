# About

The repository serves as a Jenkins integration sample for C++ projects. Jenkins build is done via Docker and C++ project itself is handled by CMake.

## Jenkins job

Create *Multibranch Pipeline* item with a name `jenkins_shared`. In *General* section set *Display Name* to `jenkins_shared`, *Description* to *CMake CTest sample with docker and Jenkins integration.*  

Click to *Add source* button in *Branch Sources* section and pick *Git*. Set *Project Repository* to `https://gitlab.com/samples9032313/jenkins_shared.git`, then in *Discover branches* click to *Add* and pick *Filter by name (with wildcards)* and set to `main`.

> **note**: `jenkins_shared` is public repository so we do not need to set any credentials there

(Optional) In *Scan Multibranch Pipeline Triggers* section check *Periodically if not otherwise run* checkbox and set to 1 hour.

Click to *Save* button.

After scanning repository is done we should start to see `main` branch in the `jenkins_shared` job list of branches.

We can click to `main` to open the branch and build it by clickind to *Build Now* button from left side menu.

## Local build

Build with

```bash
cmake -B build -S .
cmake --build build -j16
```

commands.

Run sample with

```console
$ ./build/hello 
hello!
```

command.
